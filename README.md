Arduino MIDI controller
====

Arduino Pro Micro MIDI controller.

* Two switches, latching in one direction, momentary in the other
* One foot-pedal jack (stereo, connects to Boss FS6)
* Four pots via ADS1115 break-out
* MIDI DIN output, from USB MIDI

Mounted in a guitar pedal case. Thrown together in a few nights Aug 2021.

Missing an expression pedal input. Maybe I'll add that with an analogue input.

## Lessons

* The ADS1115 doesn't have a reference voltage input. It measures relative to some exact voltage, and I don't have an exact voltage at the top of my pots. So I was getting some weird scale value that couldn't simply be bitshifted into the 0..127 range. I made a 770 step lookup table, since that's what I was seeing after shifting right by 5. Why 5? Well I wasn't thinking so I was just mashing and trying to get it to be a nice 7 bit number, and when I realized I wanted a lookup table, 5 was the most recent number I'd tried. So it stayed. Could probably halve it if needed, but there's lots of flash left, so meh.
* The ADS1115 gave very noisy readings - successive readings would move several hundred values without touching the knobs. Maybe my power scheme is bad (everything connected to two strips of copper tape on cardboard), or something needs more filtering, or maybe that's just how it is. But it was more noisy than I thought. After the lookup table the knobs mostly don't jitter, though.
* The scanning is six digitalRead-s and four reads of the I2C ADC. This was (I think) taking up too much time and throwing off the USB MIDI timing, so I had to slow the scan rate right down. At 1 scan for every 20 USB MIDI reads, MIDI timing is subjectively acceptable.
* The USB MIDI library initializes with soft Thru on for some reason? It seemed like it was even more wonky before turning that off.
* Best lesson: I can solder something like this together and have the hardware working on the first try. Three cheers for me.

## Hardware pictures

[Inside](pics/inside.jpg)

[Front](pics/front.jpg)
