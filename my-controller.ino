#include <MIDI.h>
#include <USB-MIDI.h>

//#include <pitchToNote.h>
//#include <MIDIUSB.h>
//#include <pitchToFrequency.h>
//#include <frequencyToNote.h>
//#include <MIDIUSB_Defs.h>

#include <Adafruit_ADS1X15.h>

#include "lookup.h"

MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);
USBMIDI_CREATE_INSTANCE(0, USBMIDI);

class Latch {
  private:
    bool wasDown = false;
    int pin;
    int note;

  public: 
    Latch(int _pin, int _note) {
      pin = _pin;
      note = _note;
      pinMode(pin, INPUT_PULLUP);
    }

  void handle() {
    int state = digitalRead(pin);
    if (state == LOW && !wasDown) {
      wasDown = true;
      // downEvent();
      USBMIDI.sendNoteOn(note, 127, 1);
    }
    if (state == HIGH && wasDown) {
      wasDown = false;
      // upEvent();
      USBMIDI.sendNoteOff(note, 0, 1);
    }
  }
};

class Pedal {
  private:
    bool wasDown = false;
    bool sent = false;
    int pin;
    int note;

  public: 
    Pedal(int _pin, int _note) {
      pin = _pin;
      note = _note;
      pinMode(pin, INPUT_PULLUP);
    }

  void handle() {
    if (sent) {
      sent = false;
      USBMIDI.sendNoteOff(note, 0, 1);
    }

    int state = digitalRead(pin);
    if (state == LOW && !wasDown) {
      wasDown = true;
      // downEvent();
      sent = true;
      USBMIDI.sendNoteOn(note, 127, 1);
    }
    if (state == HIGH && wasDown) {
      wasDown = false;
      // upEvent();
    }
  }
};

class FourKnobs {
  private:
    int _cc;
    int old_values[4];
    int divisor = 100;
    Adafruit_ADS1115 *adc;

  public:
    FourKnobs(int addr, int cc) {
      _cc = cc;

      adc = new Adafruit_ADS1115();
      adc->begin(addr);
      adc->setGain(GAIN_TWOTHIRDS);
    }
    
    void handle(void) {
      for (int i = 0; i<4; i++) {
        int16_t reading = adc->readADC_SingleEnded(i);
        int16_t index = reading>>5;
        if (index >= 770) index = 769;
        if (index <= 0) index = 0;
        int value = pgm_read_byte_near(lookup + index);
        if (value != old_values[i]) {
          USBMIDI.sendControlChange(_cc+i, value, 1);
        }
        old_values[i] = value;
      }
    }
};

USING_NAMESPACE_MIDI;
typedef USBMIDI_NAMESPACE::usbMidiTransport __umt;
typedef MIDI_NAMESPACE::MidiInterface<__umt> __ss;

__umt usbMIDI(0); // cableNr
__ss MIDICoreUSB((__umt&)usbMIDI);

typedef Message<MIDI_NAMESPACE::DefaultSettings::SysExMaxSize> MidiMessage;

class MIDIThru {
  static void onUsbMessage(const MidiMessage& message) {
    MIDI.send(message.type, message.data1, message.data2, message.channel);
  }
  
  public:
    MIDIThru() {
      MIDI.begin();
      MIDI.turnThruOff();
      USBMIDI.turnThruOff();
      USBMIDI.setHandleMessage(MIDIThru::onUsbMessage);
    }

    handle() {
      MIDI.read();
      USBMIDI.read();
    }
};

Pedal *p1;
Pedal *p2;
Latch *l1u;
Latch *l1d;
Latch *l2u;
Latch *l2d;
FourKnobs *knobs;
MIDIThru *thru;

void setup() {
  p1 = new Pedal(8, 60);
  p2 = new Pedal(9, 62);

  l1u = new Latch(5, 64);
  l1d = new Latch(4, 65);
  l2u = new Latch(7, 66);
  l2d = new Latch(6, 67);

  knobs = new FourKnobs(0x48, 60);

  thru = new MIDIThru();
}

int divisor = 20;
void loop() {
  if (divisor-- == 0) {
    p1->handle();
    p2->handle();
  
    l1u->handle();
    l1d->handle();
    l2u->handle();
    l2d->handle();
  
    knobs->handle();
    divisor = 20;
  }

  thru->handle();
}
